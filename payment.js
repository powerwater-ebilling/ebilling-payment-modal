// Values from hidden input field
var crn = document.getElementById("crn").value;
var amountDollars = document.getElementById("amountDollars").value;
var amountCents = document.getElementById("amountCents").value;
var form = document.getElementById("pwc-payment-form");
if (cents == "") {
    cents = "00";
}
var amount = amountDollars + "." + amountCents;


// dynamically create CC expiry year and append it to select input
function createExpiryYears() {
    var year = new Date().getFullYear();
    var $year_input = $("#EPS_EXPIRYYEAR").empty();

    for (var i = 0; i < 15; i++) {
        $("<option>")
            .val(year + i)
            .text(year + i)
            .appendTo($year_input);
    }
}


// var form = document.getElementById('pwc-payment-form');

createExpiryYears();
checkServerStatus();

// ensures certain inputs cannot use the scroll wheel to increase or decrease number value
document.addEventListener("wheel", function(event) {
    if (
        document.activeElement.type === "number" &&
        document.activeElement.classList.contains("noscroll")
    ) {
        document.activeElement.blur();
    }
});

// Ensures characters cannot be entered into number inputs or notext inputs
$('input[type="number"], .notext').on("keydown", function(e) {
    var key = window.event ? event.keyCode : event.which;

    var keys = [8, 37, 39, 46, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105];

    var x = keys.indexOf(key) > -1;

    if (x != true) {
        if (isNaN(String.fromCharCode(key))) {
            return false;
        }
    }
});


// var xhttp = new XMLHttpRequest();

// xhttp.onreadystatechange = function() {

//     if (this.readyState === 4) {
//         if (this.status !== 200) {

//             // if server is unavailable, show alternative options for payment form

//             var qualifierWrapper = document.querySelector('.qualifier-questions__wrapper');
//             var qualifierSelect = document.getElementById('qualifier-questions');

//             // set new value to show accordion of alternative options
//             qualifierSelect.options[1].value = '4';
//             qualifierSelect.options[4].value = '4';

//             // ensures payment form cannot be accessed if server is offline
//             var paymentForm = document.getElementById('pwc-payment-form');
//             $(paymentForm).remove();

//         }
//     }

// };


// // Generate fingerprint
// xhttp.open("POST", "https://www.powerwater.com.au/customers/online-services/Pay-online/fingerprint", true);


// // Submit data
// xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
// xhttp.send("crn=" + crn + "&amount=" + amount + "&api_key=ceed52a289ae7e899ce10ae202ba5249d");

// xhttp.onreadystatechange = function() {

//     if (this.readyState === 4) {
//         if (this.status === 200) {

//             var data = JSON.parse(this.responseText);

//             // error handling
//             if (!data || data.status !== true) {
//                 document.getElementById('payment-submit').disabled = true;
//                 form.insertAdjacentHTML('afterend', '<div class="mt-2 alert alert-danger id="pwc-payment-form-alert">Unable to connect to payment server. Please <a href="./?a=942">Contact us</a> to arrange payment or try again later.</div>');
//                 return;
//             }

//             form.insertAdjacentHTML('afterbegin', '<input type="hidden" name="EPS_REDIRECT" value="true">');
//             form.insertAdjacentHTML('afterbegin', '<input type="hidden" name="EPS_RESULTPARAMS" value="true">');
//             form.insertAdjacentHTML('afterbegin', '<input type="hidden" name="EPS_FINGERPRINT" value="' + data.fingerprint + '">');
//             form.insertAdjacentHTML('afterbegin', '<input type="hidden" name="EPS_TIMESTAMP" value="' + data.timestamp + '">');
//             form.insertAdjacentHTML('afterbegin', '<input type="hidden" name="EPS_TXNTYPE" value="' + data.transaction_type + '">');
//             form.insertAdjacentHTML('afterbegin', '<input type="hidden" name="EPS_MERCHANT" value="' + data.merchantid + '">');
//             // form.insertAdjacentHTML('afterbegin', '<input type="hidden" name="EPS_RESULTURL" value="' + data.url + '">');
//             form.insertAdjacentHTML('afterbegin', '<input type="hidden" name="EPS_RESULTURL" value="http://127.0.0.1:5500/ebill%20code.html">');
//             form.insertAdjacentHTML('afterbegin', '<input type="hidden" name="EPS_AMOUNT" value="' + amount + '">');
//             form.insertAdjacentHTML('afterbegin', '<input type="hidden" name="amount" value="' + amount + '">');
//             form.insertAdjacentHTML('afterbegin', '<input type="hidden" name="formatdate" value="' + data.formatdate + '">');
//         } else {
//             document.getElementById('payment-submit').disabled = true;
//             form.insertAdjacentHTML('afterend', '<div class="mt-2 alert alert-danger id="pwc-payment-form-alert">Unable to connect to payment server. Please <a href="./?a=942">Contact us</a> to arrange payment or try again later.</div>');
//         }
//     }
// };




// Checks if the Payment API server is available


function checkServerStatus() {

    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() {

        if (this.readyState === 4) {
            if (this.status !== 200) {

                // if server is unavailable, show alternative options for payment form

                var qualifierWrapper = document.querySelector('.qualifier-questions__wrapper');
                var qualifierSelect = document.getElementById('qualifier-questions');

                // set new value to show accordion of alternative options
                qualifierSelect.options[1].value = '4';
                qualifierSelect.options[4].value = '4';

                // ensures payment form cannot be accessed if server is offline
                var paymentForm = document.getElementById('pwc-payment-form');
                $(paymentForm).remove();

            }
        }

    };

    xhttp.open("GET", "https://www.powerwater.com.au/customers/online-services/Pay-online/checkstatus?checkstatus=true", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send();

}


// ensures certain inputs cannot use the scroll wheel to increase or decrease number value
document.addEventListener("wheel", function(event) {
    if (document.activeElement.type === "number" && document.activeElement.classList.contains("noscroll")) {
        document.activeElement.blur();
    }
});



function setupValidation(form) {

    $.validator.addMethod('validateCC', function(value, element) {
        var today = new Date();
        var thisYear = today.getFullYear();
        var expMonth = $(element).closest('.sq-form-question-answer').find('#EPS_EXPIRYMONTH').val();
        var expYear = $(element).closest('.sq-form-question-answer').find('#EPS_EXPIRYYEAR').val();

        return (expMonth >= 1 &&
            expMonth <= 12 &&
            (expYear >= thisYear && expYear < thisYear + 20) &&
            (expYear == thisYear ? expMonth >= (today.getMonth() + 1) : true))
    });

    // $.validator.addMethod('validateCrn', function(value, element) {
    //     return validateCrn(value);
    // });

    // $.validator.addMethod('validateCents', function(value, element) {
    //     return validateCents(value);
    // });

    var validator = $(form).validate({
        submitHandler: function(form, event) {
            console.log('validate');
            event.preventDefault();
            var valid = $(form).validate().checkForm();
            if (valid) {
                if ($('.pwc-payment-form__confirmation').length > 0) {
                    $('#payment-submit').prop('disabled', true); // stop submit button being clicked more than once
                    submitPayment(event);
                } else {
                    // $('#payment-confirmation').prop('disabled', true); // stop submit button being clicked more than once
                    // confirmationPayment(form, event);
                    $('#payment-submit').prop('disabled', true); // stop submit button being clicked more than once
                    submitPayment(event);
                }
            }
        },

        errorClass: 'mt-2 alert alert-danger',

        errorPlacement: function(error, element) {
            if (element.attr('name') == 'cents' || element.attr('name') == 'dollars' || element.attr('name') == 'EPS_EXPIRYMONTH') {
                error.appendTo(element.closest('.sq-form-question'));
            } else if (element.is('[name="EPS_CARDNUMBER"]')) {
                error.insertAfter(element.closest('.sq-form-question-answer'));
            } else {
                error.insertAfter(element);
            }
        },

        onkeyup: false,

        highlight: function(element, errorClass, validClass) {
            $(element).addClass('sq-form-error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('sq-form-error');
        },

        groups: {
            ccexp: "EPS_EXPIRYMONTH EPS_EXPIRYYEAR"
        },

        rules: {
            EPS_EXPIRYMONTH: {
                validateCC: true
            },
            EPS_EXPIRYYEAR: {
                validateCC: true
            }
        },
        messages: {
            EPS_REFERENCEID: {
                required: 'This is a required field.',
                validateCrn: 'This does not appear to be a valid customer reference number.',
                minlength: 'The CRN must be 17 characters long.',
                maxlength: 'The CRN must be 17 characters long.'
            },
            EPS_CARDNUMBER: {
                required: 'This is a required field.'
            },
            EPS_CCV: {
                required: 'This is a required field.'
            },
            cents: {
                required: 'This is a required field.',
                validateCents: 'Please enter 2 digits for the cents value.'
            },
            dollars: {
                required: 'This is a required field.',
                min: "Please enter an amount between $20 and $5,000.",
                max: "Please enter an amount between $20 and $5,000."
            },
            EPS_EXPIRYMONTH: {
                validateCC: "This is an invalid expiry date."
            },
            EPS_EXPIRYYEAR: {
                validateCC: "This is an invalid expiry date."
            }
        }

    });

}

setupValidation(form);

function submitPayment(event) {

    var form = document.getElementById('pwc-payment-form');

    // var crn = event.target.EPS_REFERENCEID.value;

    // var dollars = event.target.dollars.value;
    // var cents = event.target.cents.value;

    // append 00 if cents is left empty
    if (cents == '') {
        cents = '00';
    }

    // concatenate cents and dollars in order for direcpost to accept the amount
    // var amount = event.target.dollars.value + '.' + event.target.cents.value;

    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() {

        if (this.readyState === 4) {
            if (this.status === 200) {

                var data = JSON.parse(this.responseText);

                // error handling
                if (!data || data.status !== true) {
                    document.getElementById('payment-submit').disabled = true;
                    form.insertAdjacentHTML('afterend', '<div class="mt-2 alert alert-danger id="pwc-payment-form-alert">Unable to connect to payment server. Please <a href="./?a=942">Contact us</a> to arrange payment or try again later.</div>');
                    return;
                }

                form.insertAdjacentHTML('afterbegin', '<input type="hidden" name="EPS_REDIRECT" value="true">');
                form.insertAdjacentHTML('afterbegin', '<input type="hidden" name="EPS_RESULTPARAMS" value="true">');
                form.insertAdjacentHTML('afterbegin', '<input type="hidden" name="EPS_FINGERPRINT" value="' + data.fingerprint + '">');
                form.insertAdjacentHTML('afterbegin', '<input type="hidden" name="EPS_TIMESTAMP" value="' + data.timestamp + '">');
                form.insertAdjacentHTML('afterbegin', '<input type="hidden" name="EPS_TXNTYPE" value="' + data.transaction_type + '">');
                form.insertAdjacentHTML('afterbegin', '<input type="hidden" name="EPS_MERCHANT" value="' + data.merchantid + '">');
                form.insertAdjacentHTML('afterbegin', '<input type="hidden" name="EPS_RESULTURL" value="' + data.url + '">');
                // form.insertAdjacentHTML('afterbegin', '<input type="hidden" name="EPS_RESULTURL" value="http://127.0.0.1:5500/ebill%20code.html">');
                form.insertAdjacentHTML('afterbegin', '<input type="hidden" name="EPS_AMOUNT" value="' + amount + '">');
                form.insertAdjacentHTML('afterbegin', '<input type="hidden" name="amount" value="' + amount + '">');
                form.insertAdjacentHTML('afterbegin', '<input type="hidden" name="formatdate" value="' + data.formatdate + '">');

                // submit all values to direct post
                form.submit();
            } else {
                document.getElementById('payment-submit').disabled = true;
                form.insertAdjacentHTML('afterend', '<div class="mt-2 alert alert-danger id="pwc-payment-form-alert">Unable to connect to payment server. Please <a href="./?a=942">Contact us</a> to arrange payment or try again later.</div>');
            }
        }
    };

    // Generate fingerprint
    xhttp.open("POST", "https://www.powerwater.com.au/customers/online-services/Pay-online/fingerprint", true);

    // Submit data
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("crn=" + crn + "&amount=" + amount + "&api_key=ceed52a289ae7e899ce10ae202ba5249d");

}


function paymentResults() {

    // IE11 new URL polyfill
    function getQueryString(query) {

        var key = false,
            res = {},
            itm = null;
        var qs = location.search.substring(1);
        if (arguments.length > 0 && arguments[0].length > 1)
            key = arguments[0];
        var pattern = /([^&=]+)=([^&]*)/g;
        while (itm = pattern.exec(qs)) {
            if (key !== false && decodeURIComponent(itm[1]) === key)
                return decodeURIComponent(itm[2]);
            else if (key === false)
                res[decodeURIComponent(itm[1])] = decodeURIComponent(itm[2]);
        }
        return key === false ? res : null;

    }

    // Iterates through response codes until it finds a match and retrieves relevant restext
    function checkResponseCodes(rescode) {

        var responseCodes = [{
                id: [00, 08, 11],
                text: "Thank you for your payment."
            },
            {
                id: [01, 02, 06, 07, 12, 13, 14, 15, 19, 20, 21, 22, 25, 26, 27, 28, 29, 33, 34, 35, 36, 37, 38, 39, 40, 51, 52, 53, 54, 55, 56, 57, 59, 61, 62, 64, 65, 68, 91, 92, 93, 94, 95, 100, 101, 102, 103, 106, 109, 114, 132, 162],
                text: "Your payment has been declined. Please contact your bank for more information."
            },
            {
                id: [03, 04, 05, 09, 10, 17, 18, 23, 24, 30, 31, 32, 41, 42, 44, 67, 75, 86, 87, 88, 89, 90, 96, 97, 98, 99, 110, 111, 112, 113, 115, 116, 117, 118, 119, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 146, 157, 158, 159, 163, 164, 165, 166, 167, 168, 169, 179, 505, 510, 511, 512, 513, 514, 515, 516, 517, 524, 545, 550, 575, 577, 580, 594, 595],
                text: "Your payment has been declined. Please contact your bank for more information."
            },
            {
                id: [43, 58, 60, 63, 66, 104, 123, 124, 125, 126, 131, 145, 147, 148, 149, 151, 152, 153, 160, 175, 176, 177, 178, 190, 195, 199, 504],
                text: "Your payment has been declined. Please contact your bank for more information."
            }
        ];

        for (var i = 0; i < responseCodes.length; i++) {
            for (var j = 0; j < responseCodes[i].id.length; j++) {
                if (responseCodes[i].id[j] == rescode) {
                    restext = responseCodes[i].text;
                    return restext;
                }
            }
        }

    }

    function paymentSuccess(paymentImage, paymentTitle, paymentLogo) {

        paymentLogo.style.display = "block";

        paymentImage.insertAdjacentHTML('afterbegin', '<img width="56" height="56" alt="success-tick" src="./?a=3889">');
        paymentTitle.innerHTML = "Payment Successful!";

    }

    function paymentDecline(fingerprintMatch, restext, paymentImage, paymentTitle, paymentInfo, paymentWrapper) {

        var buttonWrapperError = document.querySelector('.button-wrapper--error');
        var paymentTable = document.querySelector('.payment-table');

        paymentImage.insertAdjacentHTML('afterbegin', '<img width="56" height="56" alt="decline-exclamation" src="./?a=10272">');
        paymentTitle.innerHTML = "Payment Declined!";
        paymentInfo.classList.add('payment--decline');

        // Fingerprint allows us to make sure the payment was legitimate. If it doesn't match return an error.
        if (fingerprintMatch != 1) {
            paymentInfo.innerHTML = 'Your payment has been declined. Please contact your bank for more information.';
        } else {
            paymentInfo.innerHTML = restext;
        }


        paymentTable.style.display = 'none';
        paymentWrapper.style.display = 'block';
        buttonWrapperError.style.display = 'block';

    }

    function paymentInvalidFingerprint(fingerprintMatch, restext, paymentImage, paymentTitle, paymentInfo, paymentWrapper) {

        var buttonWrapperError = document.querySelector('.button-wrapper--error');
        var paymentTable = document.querySelector('.payment-table');

        paymentImage.insertAdjacentHTML('afterbegin', '<img width="56" height="56" alt="decline-exclamation" src="./?a=10272">');
        paymentTitle.innerHTML = "Payment Declined!";
        paymentInfo.classList.add('payment--decline');

        // Fingerprint allows us to make sure the payment was legitimate. If it doesn't match return an error.
        if (fingerprintMatch != 1) {
            paymentInfo.innerHTML = 'Something has gone wrong. Please call us on 1800 245 092.';
        } else {
            paymentInfo.innerHTML = restext;
        }


        paymentTable.style.display = 'none';
        paymentWrapper.style.display = 'block';
        buttonWrapperError.style.display = 'block';

    }

    function paymentNoFingerprint(paymentImage, paymentTitle, paymentInfo, paymentWrapper) {

        var buttonWrapperError = document.querySelector('.button-wrapper--error');
        var paymentTable = document.querySelector('.payment-table');

        paymentImage.insertAdjacentHTML('afterbegin', '<img width="56" height="56" alt="decline-exclamation" src="./?a=10272">');
        paymentTitle.innerHTML = "Payment Issue!";
        paymentInfo.classList.add('payment--decline');

        paymentInfo.innerHTML = 'Something has gone wrong. Please call us on 1800 245 092.';

        paymentTable.style.display = 'none';
        paymentWrapper.style.display = 'block';
        buttonWrapperError.style.display = 'block';


    }



    // Retrieves data from server and displays it in a table for the user
    function generatePaymentMarkup(restext, paymentWrapper, paymentInfo) {

        var paymentTable = document.querySelector('.payment-table');
        var buttonWrapperSuccess = document.querySelector('.button-wrapper--success');

        var crn = getQueryString('crn');
        var date = getQueryString('date').replace(/\+/g, ' ');
        var amount = Number(getQueryString('amount')).toLocaleString('en-us', {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        });
        var receipt = getQueryString('receipt');

        var results = {
            'Customer reference number': crn,
            'Date and time': date,
            'Amount': '$' + amount,
            'Receipt number': receipt
        };

        var keys = Object.keys(results);

        // Creates information table
        for (var i = 0; i < keys.length; i++) {
            var cellKey = document.createElement('div');
            cellKey.className = 'payment-cell';
            cellKey.innerHTML = keys[i];
            paymentTable.appendChild(cellKey);

            var cellValue = document.createElement('div');
            cellValue.className = 'payment-cell';
            cellValue.innerHTML = results[keys[i]];
            paymentTable.appendChild(cellValue);

        }

        paymentInfo.classList.add('payment--success');
        paymentInfo.innerHTML = restext;

        paymentTable.insertAdjacentHTML('afterend', '<p class="payment-abn">Issued by Power and Water Corporation ABN 15 947 352 360</p>');

        buttonWrapperSuccess.style.display = 'block';
        paymentWrapper.style.display = 'block';

    }


    var paymentWrapper = document.querySelector('.payment-wrapper');



    // Retrieve initial queries from url
    var fingerprintMatch = getQueryString('fingerprint_match');
    var rescode = getQueryString('rescode');
    var restext = checkResponseCodes(rescode);
    console.log("dsd");
    var paymentLogo = document.getElementById('payment-result__logo');
    var paymentImage = document.querySelector('.payment-image');
    var paymentTitle = document.querySelector('.payment-title');
    var paymentInfo = document.querySelector('.payment-info');

    // Validation checks
    if (fingerprintMatch == 1) {

        if (rescode == 00 || rescode == 08 || rescode == 11) {

            paymentSuccess(paymentImage, paymentTitle, paymentLogo);
            generatePaymentMarkup(restext, paymentWrapper, paymentInfo);

        } else {

            paymentDecline(fingerprintMatch, restext, paymentImage, paymentTitle, paymentInfo, paymentWrapper);

        }

    } else if (fingerprintMatch == 0) {

        paymentInvalidFingerprint(fingerprintMatch, restext, paymentImage, paymentTitle, paymentInfo, paymentWrapper);

    } else {

        paymentNoFingerprint(paymentImage, paymentTitle, paymentInfo, paymentWrapper);




    }

    document.getElementById('payment-receipt').addEventListener('click', function(event) {
        window.print();
    });





}